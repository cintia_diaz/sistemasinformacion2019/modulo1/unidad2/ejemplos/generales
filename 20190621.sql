﻿USE ciclistas;

/*
  proyeccion
*/

SELECT c.nombre FROM ciclista c; 
-- c es un alias de la tabla, por si hay un error de ambigüedad

select c.nombre from ciclista as c; -- as es opcional

SELECT ciclista.nombre FROM ciclista;

SELECT nombre FROM ciclista;

-- para que sea una proyeccion hay que poner el distinct porque nombre no es un campo indexado sin duplicados
-- entonces puede haber nombre repetidos. La proyeccion nunca repite registros. El álgebra no admite repetidos

SELECT DISTINCT nombre FROM ciclista;


/*
  seleccion
*/
SELECT * FROM ciclista c WHERE c.edad < 30;


/*
  combino ambos operadores
*/

SELECT c.nombre FROM ciclista c WHERE c.edad<30; -- no se corresponde con la consulta del algebra relacional porque salen repetidos, nombre no es PK

SELECT DISTINCT c.nombre FROM  ciclista c WHERE c.edad<30;


-- indicame las edades de los ciclistas de banesto
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = 'Banesto';

-- los nombres de los equipos de los ciclistas que tienen menos de 30 años
SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad<30;

/*
  and y or
*/

-- listar los nombres de los ciclistas cuya edad está entre 30 y 32 (inclusive)

SELECT DISTINCT c.nombre 
FROM ciclista c 
WHERE c.edad>=30 AND c.edad<=32;

-- listar los equipos que tengan ciclistas menores de 31 añosy que su nombre comience por M

SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad < 31 AND c.nombre LIKE 'M%';

-- listar los equipos que tengan ciclistas menores de 31 añosy que su nombre comience por M

SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad < 31 OR c.nombre LIKE 'M%';
-- calculo relacional de tuplas

-- listarme los ciclistas de banesto y de kelme
SELECT * FROM  ciclista c WHERE c.nomequipo = 'Banesto' OR c.nomequipo = 'Kelme';


-- modificar con los operaradores extendidos

  -- listar los nombres de los ciclistas cuya edad está entre 30 y 32 (inclusive)
SELECT DISTINCT c.nombre FROM ciclista c WHERE c.edad BETWEEN 30 AND 32;


  -- listarme los ciclistas de banesto y de kelme
SELECT * FROM ciclista c WHERE c.nomequipo IN ('Banesto', 'Kelme');

SELECT * FROM ciclista c WHERE c.nomequipo NOT IN ('Banesto', 'Kelme');


