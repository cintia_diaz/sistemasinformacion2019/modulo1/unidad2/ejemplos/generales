﻿-- consultas con grupos

/*
Cuántas etapas ha ganado cada ciclista 

  dorsal, numeroEtapas
  1,3
  2,1
*/

  SELECT e.dorsal, COUNT(*) numeroEtapas FROM etapa e GROUP BY e.dorsal;

/*
  nombre de los ciclistas que han ganado
  más de 2 etapas
*/

  SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>2;

  SELECT c.nombre FROM ciclista c 
    JOIN   (
    SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>2
  ) c1
    USING (dorsal);

 -- otra manera (ramon)
 -- si en la tabla utilizas el total para filtrar es un having
 -- si el where es despues de agrupar es un having, un having es un where despues de agrupar

  SELECT e.dorsal, COUNT(*) numeroEtapas FROM etapa e GROUP BY e.dorsal HAVING numeroEtapas>2;

  -- como solo quiero el dorsal, me cargo el numero de etapas
  SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>2;


  /*
    el nombre del ciclista que tiene más edad
  */

    -- así no
    SELECT * FROM ciclista c ORDER BY c.edad DESC LIMIT 1;


    -- se hace con una consulta de totales
    -- c1: edad máxima 
    SELECT MAX(c.edad) maxima FROM ciclista c; 
    /*
      el nombre aquí no se pone porque juntas una consulta de totales
      con una proyeccion
    */

      -- solución 1: join

      SELECT c.nombre FROM ciclista c 
        JOIN (SELECT MAX(c.edad) maxima FROM ciclista c) c1
       ON c.edad=c1.maxima; -- en este caso tiene que ser on para que los campos sean iguales
      
      -- solución 2: where

      SELECT c.nombre FROM ciclista c WHERE c.edad=(SELECT MAX(c.edad) FROM ciclista c);

