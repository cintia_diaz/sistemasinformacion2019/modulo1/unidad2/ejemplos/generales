USE concursos;

-- Consultas hechas en grupo. Roman y Cintia

-- Consulta 1
-- Concursantes nacidos en 1981
SELECT DISTINCT c.nombre FROM concursantes c WHERE year(c.fechaNacimiento)=1981;

-- Consulta 2
-- Provincias con m�s de un concursante
SELECT provincia FROM concursantes c
  GROUP BY c.provincia
  HAVING COUNT(*)>1;

-- Consulta 3
-- Nombres de concursantes nacidos entre 1978 y 1987 y de provincias con m�s de un concursante
SELECT c.nombre FROM concursantes c  WHERE year(c.fechaNacimiento) BETWEEN 1978 AND 1987
  GROUP BY c.provincia
  HAVING COUNT(*)>1;


-- Consulta 4
-- Altura media de los concursantes de Santander
SELECT AVG(altura) FROM concursantes c WHERE poblacion='Santander';

-- Consulta 5
-- Altura media de los concursantes de Santander nacidos entre 1978 y 1987 agrupados por provincia con m�s de 1 concursante
SELECT AVG(altura) FROM concursantes c  
  WHERE poblacion='Santander' AND year(c.fechaNacimiento) BETWEEN 1978 AND 1987
  GROUP BY c.provincia
  HAVING COUNT(*)>1;