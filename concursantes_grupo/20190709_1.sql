USE concursos;

-- Consultas de Ramon sobre la bd de concursos

-- Consulta 1
-- Indicar los concursantes que tienen m�s de 90 kg y mide menos de 180
SELECT * FROM concursantes c WHERE peso >90 AND c.altura<180;

-- Consulta 2
-- Indicar la provincia que tenga m�s de 2 poblaciones
SELECT c.provincia FROM concursantes c GROUP BY c.provincia HAVING COUNT(c.poblacion)>2;


-- Consulta 3
-- Indicar el a�o en el que han nacido m�s de 1 concursante que sean de Cantabria
SELECT year(c.fechaNacimiento) FROM concursantes c WHERE c.provincia = 'Cantabria'  GROUP BY year(c.fechaNacimiento) HAVING COUNT(*)>1;

-- Consulta 4
-- Calcular la altura media por provincia y el peso m�ximo de los concursantes de esa provincia
SELECT provincia, AVG(c.altura), MAX(c.peso) FROM concursantes c GROUP BY c.provincia; 


-- Consulta 5
-- Indicar el mes que tenga m�s de 1 concursante de m�s de 70Kg e indicar el peso medio de los concursantes
SELECT MONTH(c.fechaNacimiento),AVG(c.peso) FROM concursantes c WHERE c.peso>70
GROUP BY MONTH(c.fechaNacimiento)
HAVING COUNT(*)>1;