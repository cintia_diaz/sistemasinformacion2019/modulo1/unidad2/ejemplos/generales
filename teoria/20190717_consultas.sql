﻿USE teoria1;

  /*
    1- ¿Cuantos coches ha alquilado el usuario 1?
  */
SELECT COUNT(*) nalquileres 
  FROM alquileres
  WHERE usuario=1;
-- estos son los alquileres del usuario1

SELECT COUNT(DISTINCT a.coche) ncoches
  FROM 
  alquileres a 
  WHERE a.usuario=1;
-- estos son los diferentes coches que el usuario1 ha alquilado


  /*
    2- Numero de alquileres por mes
  */

  SELECT
    MONTH(a.fecha) mes, COUNT(*) numero 
  FROM alquileres a 
  GROUP BY MONTH(a.fecha);


  /*
    3- Numero de usuarios por sexo
  */
    SELECT u.sexo, COUNT(*) numero
      FROM usuarios u
      GROUP BY u.sexo;

 /*
    4- Numero de alquileres de coches por color
 */
  SELECT color, COUNT(*) numero
  FROM coches c 
  JOIN alquileres a 
  ON c.codigoCoche = a.coche
  GROUP BY c.color;

  /*
    5- Numero de marcas
  */

    SELECT COUNT(DISTINCT c.marca) numero FROM coches c;

  /*
    6- Numero de marcas de coches alquilados
  */
    
    -- marcas distintas que se han alquilado
    SELECT 
      COUNT(DISTINCT c.marca) 
    FROM coches c 
    JOIN alquileres a 
    ON c.codigoCoche=a.coche;

    -- agrupacion para decir numero de coches alquilados por marcas
    SELECT c.marca, COUNT(*) numero 
      FROM coches c JOIN alquileres a ON c.codigoCoche = a.coche
      GROUP BY c.marca;