﻿USE teoria1;

/*
  1- Indicar el nombre de las marcas que se hayan
     alquilado coches
*/

  -- consulta sin optimizar 
  -- porque de alquileres coges todos los coches (los 5 registros)
  SELECT 
    DISTINCT c.marca 
  FROM alquileres a 
  JOIN coches c 
  ON a.coche = c.codigoCoche;

  -- consulta optimizada
  
  -- c1
  SELECT 
    DISTINCT a.coche 
  FROM alquileres a;

  -- consulta final
  SELECT DISTINCT c.marca
    FROM 
    (
     SELECT 
      DISTINCT a.coche 
     FROM alquileres a
    ) c1
    JOIN coches c
    ON c1.coche=c.codigoCoche;