﻿USE teoria1;

  -- 29/07/2019
/*
  1- Indicar el nombre de las marcas que se hayan
     alquilado coches
*/

  -- consulta sin optimizar 
  -- porque de alquileres coges todos los coches (los 5 registros)
  SELECT 
    DISTINCT c.marca 
  FROM alquileres a 
  JOIN coches c 
  ON a.coche = c.codigoCoche;

  -- consulta optimizada
  
  -- c1
  SELECT 
    DISTINCT a.coche 
  FROM alquileres a;

  -- consulta final
  SELECT DISTINCT c.marca
    FROM 
    (
     SELECT 
      DISTINCT a.coche 
     FROM alquileres a
    ) c1
    JOIN coches c
    ON c1.coche=c.codigoCoche;


 /*
    2- Nombre de los usuarios que hayan alquilado alguna vez coches
*/

  -- consulta sin optimizar
  SELECT 
    DISTINCT u.nombre 
  FROM alquileres a 
  JOIN usuarios u
  ON a.usuario = u.codigoUsuario;


  -- consulta optimizada
  SELECT 
    DISTINCT a.usuario 
  FROM alquileres a;

  SELECT 
    DISTINCT u.nombre
    FROM 
      (
      SELECT DISTINCT a.usuario FROM alquileres a
      ) c1
    JOIN 
    usuarios u 
    ON c1.usuario=u.codigoUsuario;

  -- 31/07/2019

  /*
    3- coches que no han sido alquilados
*/

    /*
    SELECT 
      * 
    FROM coches c 
    LEFT JOIN alquileres a 
    ON c.codigoCoche = a.coche;
    */

    -- sin optimizar
    SELECT 
      c.codigoCoche 
    FROM coches c 
    LEFT JOIN alquileres a 
    ON c.codigoCoche = a.coche 
    WHERE a.coche IS NULL;

    -- optimizada
    -- c1: coches alquilados
    SELECT 
      DISTINCT a.coche 
    FROM alquileres a;

    -- final: todos los coches menos los alquilados
    SELECT 
        c.codigoCoche 
      FROM coches c 
      LEFT JOIN 
      (
       SELECT 
         DISTINCT a.coche 
       FROM alquileres a
      ) c1
      ON c.codigoCoche=c1.coche
      WHERE c1.coche IS NULL;

    /*
      4- Usuarios que no han alquilado coches
    */

      -- sin optimizar

      SELECT u.codigoUsuario 
        FROM usuarios u
        LEFT JOIN alquileres a 
        ON u.codigoUsuario = a.usuario
        WHERE a.usuario IS NULL;


      -- optimizada
        -- c1 usuarios que han alquilado
        SELECT 
          DISTINCT a.usuario 
        FROM alquileres a;
     
        -- final
        SELECT u.codigoUsuario FROM 
          usuarios u 
          LEFT JOIN
          (
           SELECT 
              DISTINCT a.usuario 
           FROM alquileres a
          ) c1
          ON u.codigoUsuario=c1.usuario
          WHERE c1.usuario IS NULL;
            
