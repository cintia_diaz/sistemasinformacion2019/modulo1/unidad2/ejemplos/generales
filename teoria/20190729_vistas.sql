﻿USE teoria1;

/*
  1- Indicar el nombre de las marcas que se hayan
     alquilado coches
*/

-- crear la vista
CREATE OR REPLACE VIEW consulta1 AS
  SELECT 
    DISTINCT c.marca 
  FROM alquileres a 
  JOIN coches c 
  ON a.coche = c.codigoCoche;


-- ejecutar la vista
  SELECT * FROM consulta1 c;

  DROP VIEW consulta1;

  /* 
    Vamos a realizar la misma pero optimizada
 */
    -- no podemos poner una subconsulta dentro de una vista (en el from), da error (no subqueries)
    -- tenemos que crear una subvista

    -- c1Consulta1: coches alquilados
    CREATE OR REPLACE VIEW c1Consulta1 AS
      SELECT 
        DISTINCT a.coche 
      FROM alquileres a;


    -- consulta final
    CREATE OR REPLACE VIEW consulta1 AS
       SELECT DISTINCT c.marca
       FROM 
        c1Consulta1 c1
       JOIN coches c
       ON c1.coche=c.codigoCoche;

    SELECT * FROM consulta1;
    


/*
    2- Nombre de los usuarios que hayan alquilado alguna vez coches
*/

  -- sin optimizar
  CREATE OR REPLACE VIEW consulta2 AS
    SELECT 
    DISTINCT u.nombre 
  FROM alquileres a 
  JOIN usuarios u
  ON a.usuario = u.codigoUsuario;

  SELECT * FROM consulta2;

  -- optimizada
  CREATE OR REPLACE VIEW c1Consulta2 AS
    SELECT 
    DISTINCT a.usuario 
  FROM alquileres a;

  CREATE OR REPLACE VIEW consulta2 AS
    SELECT 
    DISTINCT u.nombre
    FROM 
      c1Consulta2 c2
    JOIN 
    usuarios u 
    ON c2.usuario=u.codigoUsuario;

  SELECT * FROM consulta2;


  /*
    3- coches que no han sido alquilados
*/

    -- sin optimizar
    CREATE OR REPLACE VIEW consulta3 AS
      SELECT 
        c.codigoCoche 
      FROM coches c 
      LEFT JOIN alquileres a 
      ON c.codigoCoche = a.coche 
      WHERE a.coche IS NULL;

    SELECT * FROM consulta3;

    -- optimizada

      -- subconsulta
      CREATE OR REPLACE VIEW c1Consulta3 AS
        SELECT 
          DISTINCT a.coche 
        FROM alquileres a;



      -- vista final
      CREATE OR REPLACE VIEW consulta3 AS
        SELECT 
          c.codigoCoche 
        FROM coches c 
        LEFT JOIN 
        c1Consulta3 c1
        ON c.codigoCoche=c1.coche
        WHERE c1.coche IS NULL;

      SELECT * FROM consulta3 c;


    /*
      4- Usuarios que no han alquilado coches
    */

       -- optimizada
        -- c1 usuarios que han alquilado
      CREATE OR REPLACE VIEW c1Consulta4 AS
        SELECT 
          DISTINCT a.usuario 
        FROM alquileres a;
     
        -- final
      
      CREATE OR REPLACE VIEW consulta4 AS
        SELECT u.codigoUsuario FROM 
          usuarios u 
          LEFT JOIN c1Consulta4 c
          ON u.codigoUsuario=c.usuario
          WHERE c.usuario IS NULL;
