﻿USE pesca;

/* 
  1- Indica el nombre del club que tiene pescadores 
*/

  SELECT 
    DISTINCT c.nombre 
  FROM clubes c 
  JOIN pescadores p 
  ON c.cif=p.club_cif;

/* 
  2- indica el nombre del club que no tiene pescadores 
*/
  SELECT 
    c.nombre 
  FROM clubes c 
  LEFT JOIN pescadores p
  ON c.cif = p.club_cif 
  WHERE p.club_cif IS NULL;

  -- no hace falta el DISTINCT, porque el club que
  -- no tenga pescadores sale sólo una vez


/*
  3- indica el nombre de los cotos autorizados a algun club
*/

    SELECT DISTINCT a.coto_nombre FROM autorizados a;
    -- si juntas con un join a cotos, lo que consigues es información de más que
    -- no vamos a utilizar, no estaría optimizada
    -- DISTINCT  es porque coto_nombre no es pk ni uk, es pk pero con otro
/*
  4- Indica la provincia que tiene cotos autorizados a algun club
*/

      SELECT
        DISTINCT c.provincia 
      FROM cotos c 
      JOIN autorizados a 
      ON c.nombre = a.coto_nombre;

  -- no estaría optimizada, sería mejor quitar nombres de cotos repetidos primero en una subconsulta
/* 
  5-Indica el nombre de los cotos que no estan autorizados a ningun club 
*/

      SELECT
        c.nombre 
      FROM cotos c 
      LEFT JOIN autorizados a 
      ON c.nombre = a.coto_nombre
      WHERE a.coto_nombre IS NULL;

  
/*
  6- Indica el numero de rios por provincia con cotos
*/

      SELECT 
        c.provincia, COUNT(DISTINCT c.rio) 
      FROM cotos c 
      GROUP BY c.provincia;

/*
  7- Indica el numero de rios por provincia con cotos autorizados
*/
      SELECT 
        c.provincia, COUNT(DISTINCT c.rio) numero
      FROM cotos c 
      JOIN autorizados a 
      ON c.nombre = a.coto_nombre
      GROUP BY c.provincia;

      -- proyectando de autorizados solo coto_nombre, hacemos una optimización como subconsulta
      -- te quitas todos los cotos repetidos en distinctos clubs


/*
  8- Indica el nombre de la provincia con mas cotos autorizados
*/

        -- c1 cuento numero de cotos autorizados por provincia
    SELECT 
      c.provincia, COUNT(*) nCotos 
    FROM cotos c 
    JOIN 
      (SELECT DISTINCT coto_nombre FROM autorizados a) c1 
    ON c.nombre = c1.coto_nombre
    GROUP BY c.provincia;

    /* 
      solucion Ramon c1: numero de cotos autorizados por provincia
    */
       SELECT 
          c.provincia, COUNT(DISTINCT c.nombre) numero
       FROM cotos c 
       JOIN autorizados a ON c.nombre = a.coto_nombre    
       GROUP BY c.provincia;


      -- c2 número maximo de cotos por provincia
    SELECT MAX(c1.nCotos) maximo FROM 
      (
       SELECT 
          c.provincia, COUNT(*) nCotos 
       FROM cotos c 
       JOIN 
        (SELECT DISTINCT coto_nombre FROM autorizados a) c1 
       ON c.nombre = c1.coto_nombre
       GROUP BY c.provincia
      ) c1;

    -- final provincia con el mayor numero de cotos
    SELECT c1.provincia FROM 
      (
       SELECT 
         c.provincia, COUNT(*) nCotos 
       FROM cotos c 
       JOIN 
          (SELECT DISTINCT coto_nombre FROM autorizados a) c1 
       ON c.nombre = c1.coto_nombre
       GROUP BY c.provincia
      ) c1
      JOIN
      (
       SELECT MAX(c1.nCotos) maximo FROM 
        (
         SELECT 
           c.provincia, COUNT(*) nCotos 
         FROM cotos c 
         JOIN 
          (SELECT DISTINCT coto_nombre FROM autorizados a) c1 
           ON c.nombre = c1.coto_nombre
           GROUP BY c.provincia
          ) c1
        ) c2
      ON c1.nCotos=c2.maximo;


      
/*
  9- Indica el nombre del pescador y el nombre de su ahijado   
*/

  SELECT padrinos.nombre padrino, ahijados.nombre ahijado
    FROM pescadores padrinos 
    JOIN apadrinar a ON padrinos.numSocio = a.padrino
    JOIN pescadores ahijados ON ahijados.numSocio=a.ahijado;

/*
  10- Indica el numero de ahijados de cada pescador
*/

  SELECT 
      a.padrino, COUNT(*) numero 
    FROM apadrinar a 
    GROUP BY a.padrino;





