﻿USE paginaweb;

-- 15/07/2019. Consultas con JOIN
/*
  1 - El título del libro y nombre del autor que lo ha escrito
*/

  -- con join
  SELECT a.nombre_completo, l.titulo 
    FROM libro l JOIN autor a
  ON l.autor = a.id_autor;
   
  -- con producto cartesiano
  SELECT a.nombre_completo, l.titulo 
    FROM libro l, autor a 
    WHERE l.autor=a.id_autor;

  -- con join como producto cartesiano
  SELECT a.nombre_completo, l.titulo 
    FROM libro l JOIN autor a
  WHERE l.autor = a.id_autor;


  /*
    2 - Nombre del autor  y titulo del libro en el que han ayudado
  */

    -- mi solución
    SELECT c1.nombre_completo, l.titulo FROM (
      SELECT * FROM ayuda a JOIN autor a1 ON a.autor = a1.id_autor) c1 
      JOIN libro l 
    ON c1.libro=l.id_libro;

    -- solucion Ramón
    SELECT a1.nombre_completo, l.titulo
      FROM libro l 
      JOIN ayuda a ON l.id_libro = a.libro 
      JOIN autor a1 ON a.autor = a1.id_autor;

    -- tambien se puede hacer los join seguidos
     SELECT a1.nombre_completo, l.titulo
      FROM libro l JOIN ayuda a JOIN autor a1 
      ON l.id_libro = a.libro
      AND a.autor = a1.id_autor;
 

    /*
      3 - Los libros que se han descargado cada usuario con la fecha de descarga. 
          Listar el login, fecha descarga, id_libro
    */
      /* al final no hace falta dos tablas, en fechadescarga está toda la información
      SELECT d.usuario, d.libro, f.fecha_descarga FROM descarga d 
        JOIN fechadescarga f
        USING (libro);
      */

      SELECT f.libro,
             f.usuario,
             f.fecha_descarga
        FROM fechadescarga f;

     /* 
      4 - Los libros que se han descargado cada usuario con la fecha de descarga. 
          Listar el login, correo, fecha descarga, titulo del libro
    */

      SELECT u.login, u.email, f.fecha_descarga, l.titulo FROM fechadescarga f 
        JOIN libro l
        ON f.libro=l.id_libro
        JOIN usuario u
        ON f.usuario=u.login;

      -- si tuviera que quitar una tabla sería fechadescarga porque está tantas veces como se haya descargado el libro

      -- solucion Ramon
        SELECT u.login, u.email, f.fecha_descarga, l.titulo
          FROM fechadescarga f JOIN descarga d
          ON f.libro=d.libro AND f.usuario = d.usuario
          JOIN usuario u ON d.usuario = u.login
          JOIN libro l ON d.libro = l.id_libro;

      -- juntando todos los join, es muy tosco
        SELECT u.login, u.email, f.fecha_descarga, l.titulo
          FROM fechadescarga f JOIN descarga d JOIN usuario u JOIN libro l
          ON f.libro=d.libro AND f.usuario = d.usuario
          AND d.usuario = u.login
          AND d.libro = l.id_libro;

-- 16/07/2019
/*
  5 - El número de libros que hay
*/
  SELECT COUNT(*) nlibro FROM libro l;

/*
  6 - El número de libros por colección (no hace falta colocar nombre de la colección)
*/
  SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion;
 

/*
  7 - La colección que tiene más libros (no hace falta colocar nombre de la colección)
*/

-- version con JOIN
  -- subconsulta c1: calculo número de libros por colección
    
  SELECT l.coleccion, COUNT(*) nlibros 
  FROM libro l 
  GROUP BY l.coleccion;


  -- subconsulta c2: calculo el máximo del número de libros
  SELECT MAX(c1.nlibros) maximo FROM 
    (
      SELECT l.coleccion, COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1;

  -- consulta final: la(s) colección(es) que tiene más libros
  SELECT c1.coleccion
    FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
          SELECT MAX(c1.nlibros) maximo FROM 
             (
              SELECT l.coleccion, COUNT(*) nlibros 
              FROM libro l 
              GROUP BY l.coleccion
             ) c1
         ) c2
    ON c1.nlibros=c2.maximo;


  -- version con WHERE
    -- consulta final

    SELECT c1.coleccion FROM 
      (
      SELECT l.coleccion, COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
      ) c1
      WHERE c1.nlibros=
          (
            SELECT MAX(c1.nlibros) maximo FROM 
              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l 
                GROUP BY l.coleccion
              ) c1        
          );

    -- version con HAVING
      -- consulta final
         SELECT l.coleccion, COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
          HAVING nlibros=
           (
            SELECT MAX(c1.nlibros) maximo FROM 
              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l 
                GROUP BY l.coleccion
              ) c1
           );

        SELECT l.coleccion
          FROM libro l 
          GROUP BY l.coleccion
          HAVING COUNT(*)=
           (
            SELECT MAX(c1.nlibros) maximo FROM 
              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l 
                GROUP BY l.coleccion
              ) c1
           );




/*
  8 - La colección que tiene menos libros (no hace falta colocar nombre de la colección)
*/

  -- c1
  SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion;

  -- c2
  SELECT MIN(c1.nlibros) minimo FROM 
    (
    SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
    ) c1;

  -- final version con JOIN
  SELECT c1.coleccion FROM 
      (
        SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
      ) c1
    JOIN
      (
        SELECT MIN(c1.nlibros) minimo FROM 
        (
        SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
        ) c1
      ) c2
     ON c1.nlibros=c2.minimo;

  -- final version con WHERE
    SELECT c1.coleccion FROM 
      (
       SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
      ) c1
      WHERE nlibros=
       (
        SELECT MIN(c1.nlibros) minimo FROM 
          (
          SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
          ) c1
       );

    -- final version con HAVING
      SELECT l.coleccion
        FROM libro l 
        GROUP BY l.coleccion
        HAVING COUNT(*)=
        (
          SELECT MIN(c1.nlibros) minimo FROM 
            (
              SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
            ) c1
        );

  -- 17/07/2019

/* 
  9 -  El nombre de la coleccion que tiene más libros
*/

  SELECT c.nombre FROM (
    SELECT c1.coleccion
    FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
          SELECT MAX(c1.nlibros) maximo FROM 
             (
              SELECT l.coleccion, COUNT(*) nlibros 
              FROM libro l 
              GROUP BY l.coleccion
             ) c1
         ) c2
    ON c1.nlibros=c2.maximo
    ) consulta7
    JOIN 
    coleccion c 
    ON consulta7.coleccion=c.id_coleccion;

  

/* 
  10 -  El nombre de la coleccion que tiene menos libros
*/
  SELECT c.nombre FROM 
    (
      SELECT c1.coleccion FROM 
      (
        SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
      ) c1
    JOIN
      (
        SELECT MIN(c1.nlibros) minimo FROM 
        (
        SELECT l.coleccion, COUNT(*) nlibros  FROM libro l GROUP BY l.coleccion
        ) c1
      ) c2
     ON c1.nlibros=c2.minimo
    ) consulta8
    JOIN
    coleccion c
    ON consulta8.coleccion=c.id_coleccion;

/*
  11 -  El nombre del libro que se ha descargado más veces
*/


  -- c1: el numero de veces que se descarga cada libro (por diferentes usuarios)
  SELECT d.libro, COUNT(*) nveces FROM descarga d GROUP BY d.libro;

  -- c2: el máximo de las veces que se descarga un libro
  SELECT MAX(c1.nveces) maximo FROM (
    SELECT d.libro, COUNT(*) nveces FROM descarga d GROUP BY d.libro)
    c1;

  -- consulta final: con un JOIN, titulo del libro más descargado
  SELECT l.titulo FROM 
    (
      SELECT d.libro, COUNT(*) nveces FROM descarga d GROUP BY d.libro
    ) c1
    JOIN
    (
      SELECT MAX(c1.nveces) maximo FROM (
         SELECT d.libro, COUNT(*) nveces FROM descarga d GROUP BY d.libro)
        c1
    ) c2
    ON c1.nveces=c2.maximo
    JOIN
    libro l
    ON c1.libro=l.id_libro;

  -- solucion: hay que sacar la información de la tabla fechadescarga porque es donde se guarda la información por fecha 
  -- libros descargados por mismo usuario o por distintos usuarios

   -- c1:
   -- número de veces  que se descarga cada libro = número de descargas por cada libro
   -- (meto un libro más, el 1 para que me salga uno más)
     SELECT libro, COUNT(*) ndescargas 
      FROM fechadescarga f 
      GROUP BY f.libro;

   -- c2:
   -- el numero maximo de descargas por libro
    SELECT MAX(c1.ndescargas) maximo FROM (
       SELECT libro, COUNT(*) ndescargas 
        FROM fechadescarga f 
        GROUP BY f.libro
      ) c1;
    

   -- c3:
   -- codigo del libro que se ha descargado más veces
  
    SELECT c1.libro FROM (
        SELECT libro, COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY f.libro
      ) c1
      JOIN (
         SELECT MAX(c1.ndescargas) maximo FROM (
           SELECT libro, COUNT(*) ndescargas 
            FROM fechadescarga f 
            GROUP BY f.libro
           ) c1        
      ) c2
      ON
      c1.ndescargas=c2.maximo;

   
    -- final:
      
      SELECT l.titulo FROM libro l
        JOIN (
              SELECT c1.libro FROM (
                SELECT libro, COUNT(*) ndescargas 
                  FROM fechadescarga f 
                  GROUP BY f.libro
                  ) c1
                JOIN (
                 SELECT MAX(c1.ndescargas) maximo FROM (
                     SELECT libro, COUNT(*) ndescargas 
                      FROM fechadescarga f 
                      GROUP BY f.libro
                    ) c1        
                  ) c2
                ON
                c1.ndescargas=c2.maximo
             ) c3
        ON c3.libro=l.id_libro;      

/*
  12 -  El nombre del usuario que ha descargado más libros
*/

    -- c1:
    -- numero de libros por cada usuario
    
    SELECT f.usuario, COUNT(*) ndescargas 
      FROM fechadescarga f 
      GROUP BY f.usuario;


    -- c2: 
    -- numero maximo de descargas por usuario
    
    SELECT MAX(c1.ndescargas) maximo FROM (
        SELECT f.usuario, COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY f.usuario
        ) 
      c1;

    -- final:
    -- el usuario que ha descargado más veces
    
    -- opcion JOIN

    SELECT c1.usuario FROM (
        SELECT f.usuario, COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY f.usuario
      ) c1
      JOIN (
        SELECT MAX(c1.ndescargas) maximo FROM (
          SELECT f.usuario, COUNT(*) ndescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) 
          c1
      ) c2
      ON c1.ndescargas=c2.maximo;

    -- opcion HAVING
    
        SELECT f.usuario, COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY f.usuario
          HAVING ndescargas=(
            SELECT MAX(c1.ndescargas) maximo FROM
              (
                SELECT f.usuario, COUNT(*) ndescargas 
                  FROM fechadescarga f 
                  GROUP BY f.usuario
              ) c1
          );

-- 22/07/2019

/*
  13 -  El nombre de los usuarios que han descargado más libros que Adam3
*/

    --  c1: numero de descargas de Adam3
    SELECT 
      COUNT(*) ndescargas 
    FROM fechadescarga f 
    WHERE f.usuario='Adam3';

    -- c2: descargas que ha realizado cada usuario
      SELECT 
        f.usuario, COUNT(*) nlibros
      FROM fechadescarga f 
      GROUP BY f.usuario;

    -- consulta final
    -- con HAVING
    SELECT f.usuario  FROM fechadescarga f GROUP BY f.usuario
      HAVING COUNT(*)>(
        SELECT COUNT(*) ndescargas FROM fechadescarga f WHERE f.usuario='Adam3'     
      );

    -- con WHERE
    SELECT f.usuario, COUNT(*) ndescargas
      FROM fechadescarga f 
      GROUP BY f.usuario;

    SELECT c1.usuario FROM (
        SELECT f.usuario, COUNT(*) ndescargas
          FROM fechadescarga f 
          GROUP BY f.usuario
      ) c1
      WHERE c1.ndescargas>(
          SELECT COUNT(*) nlibros FROM fechadescarga f WHERE f.usuario='Adam3'
        )
     ;

    

/*
  14 -  El mes que más libros se han descargado
*/

    -- c1
    -- cuento los libros descargados por mes

    SELECT 
      MONTH(f.fecha_descarga),COUNT(*) ndescargas 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga);

    -- c2
    -- calculo el máximo 
    SELECT MAX(c1.ndescargas) maximo FROM 
      (
        SELECT 
           MONTH(f.fecha_descarga),COUNT(*) ndescargas 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
      ) c1;

    -- final con JOIN
    SELECT c1.mes FROM 
      (
        SELECT 
          MONTH(f.fecha_descarga) mes,COUNT(*) ndescargas 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
      ) c1
      JOIN
      (
        SELECT MAX(c1.ndescargas) maximo FROM 
         (
          SELECT 
             MONTH(f.fecha_descarga) mes,COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY MONTH(f.fecha_descarga)
          ) c1
      ) c2
      ON c1.ndescargas=c2.maximo;

    -- final con HAVING

      SELECT 
        MONTH(f.fecha_descarga) mes,COUNT(*) ndescargas 
      FROM fechadescarga f 
      GROUP BY MONTH(f.fecha_descarga)
      HAVING ndescargas=
        (
          SELECT MAX(c1.ndescargas) maximo FROM 
          (
            SELECT 
               MONTH(f.fecha_descarga),COUNT(*) ndescargas 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
          ) c1
        );




