﻿USE ciclistas;

/* 
  ciclistas que han ganado etapas
  Mostrar dorsal
*/
SELECT DISTINCT e.dorsal FROM etapa e;


/*
  ciclistas que han ganado puertos
  Mostrar dorsal
*/

SELECT DISTINCT p.dorsal FROM puerto p;

-- Operadores de conjuntos
/* 
  Dorsal de los ciclistas que han ganado etapas o puertos
  */


  SELECT DISTINCT e.dorsal FROM etapa e
    UNION
  SELECT DISTINCT p.dorsal FROM puerto p;

  -- Con union all salen los de las consultas más los repetidos
   SELECT DISTINCT e.dorsal FROM etapa e
    UNION ALL
   SELECT DISTINCT p.dorsal FROM puerto p;

  /* 
    Dorsal de los ciclistas que han ganado
    etapas y puertos
*/
 
  -- para hacer la interseccion no nos sirve INTERSECT porque en MySQL no hay
/*
   SELECT DISTINCT e.dorsal FROM etapa e
    INTERSECT
  SELECT DISTINCT p.dorsal FROM puerto p; */

    -- para hacer la intersección 

    SELECT c1.dorsalEtapa dorsal FROM  -- nos quedamos con uno de los dorsales, es igual
      (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
      JOIN
      (SELECT DISTINCT p.dorsal as dorsalPuerto FROM puerto p) c2
      ON c1.dorsalEtapa=c2.dorsalPuerto;

    -- evitamos que dbforge renombre los campos poniendo unos alias a los dorsales

/* 
    Listado de todas las etapas y
    el ciclista que las ha ganado
*/
    SELECT * FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal;


/* Listado de todos los puertos y
   el ciclista que los ha ganado
*/

    SELECT * FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal;

    -- 02/07/2019
/*
  combinaciones internas
*/
-- Listado de todos los ciclistas junto con todos los datos del equipo
-- al que pertenecen
-- inner join = join
SELECT * 
  FROM equipo e INNER JOIN ciclista c 
  ON e.nomequipo = c.nomequipo;

-- el using te quita uno de los dos nomequipo, elimina el campo duplicado, la ambigüedad

SELECT * 
  FROM equipo e JOIN ciclista c 
  USING (nomequipo);

-- no recomendable porque el where es más bien para la selección

SELECT * 
  FROM equipo e INNER JOIN ciclista c 
  WHERE e.nomequipo = c.nomequipo;


/*
  producto cartesiano
*/
-- se queda con todas las combinaciones posibles, con el join es con el que te interesa

SELECT * FROM ciclista c, equipo e;

-- convierto el producto cartesiano en una combinación interna
SELECT * 
  FROM ciclista c, equipo e
  WHERE c.nomequipo=e.nomequipo;

/*
  Listar los nombres del ciclista y del equipo de aquellos
  ciclistas que hayan ganado puertos
*/
 
  SELECT DISTINCT c.nombre, c.nomequipo             -- como no quiero repetidos, pongo DISTINCT en nombre 
    FROM ciclista c JOIN puerto p USING(dorsal);

/*
  Listar los nombres del ciclista y del equipo de aquellos
  ciclistas que hayan ganado etapas
*/

  SELECT DISTINCT c.nombre, c.nomequipo FROM etapa e JOIN ciclista c USING (dorsal);

/*
  Consulta optimizada
  */
-- c1 (subconsulta) : los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- completa
  SELECT c.nombre, c.nomequipo
    FROM ciclista c 
      JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 
      USING (dorsal);

  /*
    ciclistas que han ganado puertos y el numero de puertos que han ganado. Del ciclista quiere 
    saber el dorsal y el nombre
    dorsal, nombre, numeroPuertos
    */
-- c1
  SELECT dorsal, c.nombre, p.nompuerto FROM puerto p JOIN ciclista c USING(dorsal);
-- metiendo el nompuerto ya no hace falta DISTINCT porque no salen repetidos, son distintos puertos por eso salen más registros

-- completa
    SELECT c1.dorsal, c1.nombre, COUNT(*) numeroPuertos
      FROM (
        SELECT dorsal, c.nombre, p.nompuerto FROM puerto p JOIN ciclista c USING(dorsal)
      ) c1 
      GROUP BY c1.dorsal, c1.nombre;

-- en el select hay que poner el nombre del campo por el que agrupamos
-- y como quieres que salga nombre, también tienes que agrupar por nombre en GROUP BY
-- en cuanto pones el alias c1, los campos deberias poner con su alias c1.