﻿-- ejemplos sobre funciones de totales

  USE ciclistas;

-- numero de etapas que hay
SELECT COUNT(*) total FROM etapa e;
 
SELECT COUNT(e.numetapa) FROM etapa e;
SELECT COUNT(e.dorsal) FROM etapa e;

-- numero de maillots que hay
SELECT COUNT(*) total FROM maillot m;

-- numero de ciclistas que hayan ganado alguna etapa =
-- cuantos ciclistas han ganado etapas

-- sin subconsultas : cuenta sin repetidos
SELECT COUNT(DISTINCT e.dorsal) FROM etapa e;

-- con subconsultas
  -- c1 : dorsal de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

  -- final
SELECT COUNT(*) total FROM 
  (SELECT DISTINCT e.dorsal FROM etapa e) c1;

-- si hay nulls pondrias count(dorsal) porque count cuenta nulls tambien